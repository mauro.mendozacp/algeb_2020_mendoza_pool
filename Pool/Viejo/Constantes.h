/*#ifndef CONSTANTES_H
#define CONSTANTES_H
#include "raylib.h"

namespace pool
{
	//----------- De Pantalla -----------
	const int screenWidth = 1280;
	const int screenHeight = 720;
	const int fuenteLetra = 20;
	const int fuenteGameOver = 60;
	const char textoGameOver[] = { "GameOver" };

	//----------- Fisicas -----------
	const float raizDe3 = 1.732050f;
	const float maxVelPelota = 10.0f;				// De cualquier Pelota
	const float potenciaMaxTranferida = 0.95f;		// De Pelota a Pelota
	const float PerdidaPotencia = 0.9958f;			// Rozamiento
	const float animacionDesaparicion = 0.9f;
	const int disminucionPorcentual = 15;

	//----------- De Tablero -----------
	const int separacionBolas = 2;
	const int separacionPelotasDibujado = 26;
	const Vector2 posicionInicialBlanca = { screenWidth * 2 / 10, screenHeight * 5 / 10 };
	const Vector2 posicionInicialUno = { screenWidth * 6 / 10, screenHeight * 5 / 10 };

	//----------- De Pelota -----------
	const int maxPelotas = 16;
	const int radioGral = 20;

	//----------- De Bandas -----------
	const int maxBandas = 6;
	const int maxEsquinas = 2;

	//----------- Hoyos -----------
	const int maxHoyos = 6;
	const int radioHoyos = 36;
	const int margenH = 58;
	const int margenV = 53;
}

#endif*/