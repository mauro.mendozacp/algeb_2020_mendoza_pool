/*#include "../Pool/Merge.h"
#include <math.h>
#include <vector>

namespace pool
{
    //----------- Configurables InGame -----------
    bool gameOver = false;
    int lisasEmbocadas = 0;
    int ralladasEmbocadas = 0;

    //----- Pelotas -----
    PELOTAS pelota[maxPelotas];
    int auxRaizDe3PorRadio = 0;   // Auxiliar

    //----- Bordes -----
    BANDAS banda[maxBandas];

    //----- Hoyos -----
    HOYOS hoyo[maxHoyos];

    //----- Fondo -----
    Texture2D fondo;

    //----- Palo -----
    PALO palo;

    Vector2 mouse;
    bool mousePrecionado;

    void game()
    {
        InitWindow(screenWidth, screenHeight, "2020_Pool_Algebra");
        SetTargetFPS(60);

        init();

        while (!WindowShouldClose())
        {
            input();
            update();
            colisiones();
            draw();
        }

        CloseWindow();
    }

    void init()
    {
        initPelotas();
        initHoyos();
        initBordes();
        initPalo();
        gameOver = false;
    }

    void input()
    {
        if (algunaPelotaEnMovimiento())
        {
            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
            {
                std::cout << "mouse: " << mouse.x << " - " << mouse.y << std::endl;
            }
        }

        hacks();
        if (!gameOver)
        {
            mouse = GetMousePosition();

            if (ChequeoPuntoCirculo(mouse, pelota[0].cir, pelota[0].radio))
            {
                if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && algunaPelotaEnMovimiento())
                {
                    mousePrecionado = true;
                    std::cout << "Mouse Pos: " << mouse.x << " - " << mouse.y << std::endl;

                    palo.activo = true;
                }
            }

            if (IsMouseButtonUp(MOUSE_LEFT_BUTTON) && ChequeoPuntoCirculo(mouse, pelota[0].cir, pelota[0].radio) && mousePrecionado)
            {
                mousePrecionado = false;
                std::cout << "Suelta." << std::endl;

                palo.activo = false;
            }
            else if (IsMouseButtonUp(MOUSE_LEFT_BUTTON) && mousePrecionado && !pelota[0].enMovimiento)
            {
                palo.activo = false;
                mousePrecionado = false;
                pelota[0].enMovimiento = true;

                pelota[0].vel.x = (pelota[0].cir.x - mouse.x) / disminucionPorcentual;
                pelota[0].vel.y = (pelota[0].cir.y - mouse.y) / disminucionPorcentual;

                if (pelota[0].vel.x > maxVelPelota) pelota[0].vel.x = maxVelPelota;
                if (pelota[0].vel.y > maxVelPelota) pelota[0].vel.y = maxVelPelota;
                if (pelota[0].vel.x < -maxVelPelota) pelota[0].vel.x = -maxVelPelota;
                if (pelota[0].vel.y < -maxVelPelota) pelota[0].vel.y = -maxVelPelota;

                std::cout << "Dispara. Vel.x: " << pelota[0].vel.x << " - Vel.y: " << pelota[0].vel.y << std::endl;
            }
        }
    }

    void update()
    {
        if (!gameOver)
        {
            updatePalo();
            moverPelota();
        }
        // animacion desaparicion y gameOver
        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].desaparicion)
            {
                pelota[i].vel = { 0, 0 };
                pelota[i].radio *= animacionDesaparicion;
                pelota[i].enMovimiento = false;
                if (pelota[i].radio < 0.5f)
                {
                    pelota[i].activo = false;
                    pelota[i].desaparicion = false;
                    switch (pelota[i].tipo)
                    {
                    case TIPOPELOTA::BLANCA:
                        
                        break;
                    case TIPOPELOTA::LISA:
                        pelota[i].cir.x = 150 + 2 * radioGral * (i-1) + separacionPelotasDibujado * (i-1);
                        pelota[i].cir.y = 30;
                        pelota[i].radio = radioGral;
                        pelota[i].radio = radioGral; lisasEmbocadas++;
                        break;
                    case TIPOPELOTA::RALLADA:
                        pelota[i].cir.x = 730 + 2 * radioGral * (i - 1 - maxPelotas / 2) + separacionPelotasDibujado * (i - 1 - maxPelotas / 2);
                        pelota[i].cir.y = 30;
                        ralladasEmbocadas++;
                        pelota[i].radio = radioGral; 
                        break;
                    case TIPOPELOTA::NEGRA:
                        gameOver = true;
                        break;
                    default:
                        break;
                    }
                    std::cout << "Pelota " << i << " embocada." << std::endl;
                }
            }
        }
    }

    void draw()
    {
        BeginDrawing();
        ClearBackground(RAYWHITE);

        // ----- Mesa -----
        DrawTexture(fondo, 0, 0, WHITE);

#if DEBUG

        DrawRectangleLinesEx(banda[0].rec, 1, GREEN);
        DrawRectangleLinesEx(banda[1].rec, 1, RED);
        DrawRectangleLinesEx(banda[2].rec, 1, BLUE);
        DrawRectangleLinesEx(banda[3].rec, 1, VIOLET);
        DrawRectangleLinesEx(banda[4].rec, 1, WHITE);
        DrawRectangleLinesEx(banda[5].rec, 1, BLACK);

        // ----- Hoyos -----
        for (int i = 0; i < maxHoyos; i++)
        {
            dibujarHoyos(hoyo[i]);
            DrawCircleLines(banda[i].esq[0].cir.x, banda[i].esq[0].cir.y, banda[i].esq[0].radio, banda[i].esq[0].color);
            DrawCircleLines(banda[i].esq[1].cir.x, banda[i].esq[1].cir.y, banda[i].esq[1].radio, banda[i].esq[1].color);
        }
#endif

        for (int i = 0; i < maxPelotas; i++)
        {
            // Pelota
            DrawCircle(pelota[i].cir.x, pelota[i].cir.y, pelota[i].radio, pelota[i].color);

            if (i > 0)
            {
                DrawText(TextFormat("%i", i), (pelota[i].cir.x - (MeasureText(TextFormat("%i", i), pelota[i].radio) / 2)), pelota[i].cir.y - pelota[i].radio / 2, pelota[i].radio, WHITE);
            }

        }
        Vector2 endpos = { pelota[0].cir.x + (mouse.x - pelota[0].cir.x) / 2, pelota[0].cir.y + (mouse.y - pelota[0].cir.y) / 2 };

        if (mousePrecionado)    // arreglar el maximo de la linea 
        {
            DrawLine(pelota[0].cir.x, pelota[0].cir.y, endpos.x, endpos.y, BLACK);
        }

        // ----- Palo -----     VEER
        if (palo.activo)
        {
            float porc = 0;
            if (palo.rot > 0 && palo.rot <= 90)
            {
                porc = palo.rot * 100 / 90;
                endpos.x += (palo.textura.height / 2) * porc / 100;
            }
            if (palo.rot <= 0 && palo.rot >= -90)
            {
                porc = abs(palo.rot) * 100 / 90;
                endpos.x -= (palo.textura.height / 2) * porc / 100;
            }

            if (palo.rot < -90 && palo.rot > -180)
            {
                porc = 100 - (abs(palo.rot) - 90) * 100 / 90;
                endpos.x -= (palo.textura.height / 2) * porc / 100;
            }
            if (palo.rot <= 180 && palo.rot >= 90)
            {
                porc = 100 - (palo.rot - 90) * 100 / 90;
                endpos.x += (palo.textura.height / 2) * porc / 100;
            }

            if (palo.rot > 0 && palo.rot <= 90)
            {
                porc = 100 - palo.rot * 100 / 90;
                endpos.y -= (palo.textura.height / 2) * porc / 100;
            }
            if (palo.rot <= 0 && palo.rot >= -90)
            {
                porc = 100 - abs(palo.rot) * 100 / 90;
                endpos.y -= (palo.textura.height / 2) * porc / 100;
            }

            if (palo.rot < -90 && palo.rot > -180)
            {
                porc = (abs(palo.rot) - 90) * 100 / 90;
                endpos.y += (palo.textura.height / 2) * porc / 100;
            }
            if (palo.rot <= 180 && palo.rot >= 90)
            {
                porc = (palo.rot - 90) * 100 / 90;
                endpos.y += (palo.textura.height / 2) * porc / 100;
            }
            DrawTextureEx(palo.textura, endpos, palo.rot, 1, WHITE);
        }

        if (gameOver)
        {
            DrawText(textoGameOver, GetScreenWidth() / 2 - MeasureText(textoGameOver, fuenteGameOver) / 2, GetScreenHeight() / 2 - fuenteGameOver / 2, fuenteGameOver, BLACK);
        }

        EndDrawing();
    }

    void moverPelota()
    {
        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].activo && pelota[i].enMovimiento)
            {
                pelota[i].cir.x += pelota[i].vel.x;
                pelota[i].cir.y += pelota[i].vel.y;

                pelota[i].vel.x *= pelota[i].velDesgaste;
                pelota[i].vel.y *= pelota[i].velDesgaste;

                if (fabsf(pelota[i].vel.x) < 0.1 && fabsf(pelota[i].vel.y) < 0.1)
                {   // ----- Que pare -----
                    std::cout << "Pelota " << i << " ha parado." << std::endl;
                    pelota[i].enMovimiento = false;
                    if(algunaPelotaEnMovimiento())
                        std::cout << "Todas las pelotas han parado\n";
                }
            }
        }
    }

    void colisiones()
    {
        colisionPelotaPelota();
        colisionPelotaEsquina();
        colisionPelotaBanda();
        colisionPelotaHoyo();
    }

    void colisionPelotaPelota()
    {
        std::vector < std::pair < PELOTAS*, PELOTAS* >> listPelPel;

        float desplazamiento = 0;
        float distancia = 0;

        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].activo)
            {
                for (int j = 0; j < maxPelotas; j++)
                {
                    if (pelota[j].activo && j > i) // Exept�o el doble chequeo con las anteriores y de la pelota consigo misma
                    {
                        distancia = distanciaEntrePuntos(pelota[i].cir, pelota[j].cir);

                        if (chequearColisionCirCir(distancia, pelota[i].radio, pelota[j].radio))
                        {
                            std::cout << "ColisionPP: Pelota: " << i << " - Pelota: " << j << std::endl;

                            listPelPel.push_back({ &pelota[i], &pelota[j] });

                            desplazamiento = 0.5f * (distancia - pelota[i].radio - pelota[j].radio);

                            pelota[i].cir.x -= desplazamiento * (pelota[i].cir.x - pelota[j].cir.x) / distancia;
                            pelota[i].cir.y -= desplazamiento * (pelota[i].cir.y - pelota[j].cir.y) / distancia;

                            pelota[j].cir.x += desplazamiento * (pelota[i].cir.x - pelota[j].cir.x) / distancia;
                            pelota[j].cir.y += desplazamiento * (pelota[i].cir.y - pelota[j].cir.y) / distancia;
                        }
                    }
                }
            }
        }

        for (auto c : listPelPel)
        {
            PELOTAS* p1 = c.first;
            PELOTAS* p2 = c.second;

            distancia = distanciaEntrePuntos(p1->cir, p2->cir);;

            float nx = (p2->cir.x - p1->cir.x) / distancia;
            float ny = (p2->cir.y - p1->cir.y) / distancia;

            float tx = -ny;
            float ty = nx;

            float dpTan1 = p1->vel.x * tx + p1->vel.y * ty;
            float dpTan2 = p2->vel.x * tx + p2->vel.y * ty;

            float dpNorm1 = p1->vel.x * nx + p1->vel.y * ny;
            float dpNorm2 = p2->vel.x * nx + p2->vel.y * ny;

            float m1 = (dpNorm1 * (p1->masa - p2->masa) + 2.0f * p2->masa * dpNorm2) / (p1->masa + p2->masa);
            float m2 = (dpNorm2 * (p2->masa - p1->masa) + 2.0f * p1->masa * dpNorm1) / (p1->masa + p2->masa);

            p1->vel.x = tx * dpTan1 + nx * m1;
            p1->vel.y = ty * dpTan1 + ny * m1;
            p2->vel.x = tx * dpTan2 + nx * m2;
            p2->vel.y = ty * dpTan2 + ny * m2;

            p1->enMovimiento = true;
            p2->enMovimiento = true;
        }
    }

    void colisionPelotaEsquina()
    {
        std::vector < std::pair < PELOTAS*, ESQUINAS* >> listPelEsq;

        float desplazamiento = 0;
        float distancia = 0;

        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].activo)
            {
                for (int j = 0; j < maxBandas; j++)
                {
                    for (int k = 0; k < maxEsquinas; k++)
                    {
                        distancia = distanciaEntrePuntos(pelota[i].cir, banda[j].esq[k].cir);

                        if (chequearColisionCirCir(distancia, pelota[i].radio, banda[j].esq[k].radio))
                        {
                            std::cout << "ColisionPP: Pelota: " << i << " - Esquina: " << j << std::endl;

                            listPelEsq.push_back({ &pelota[i], &banda[j].esq[k] });

                            desplazamiento = (distancia - pelota[i].radio - banda[j].esq[k].radio);

                            pelota[i].cir.x -= desplazamiento * (pelota[i].cir.x - banda[j].esq[k].cir.x) / distancia;
                            pelota[i].cir.y -= desplazamiento * (pelota[i].cir.y - banda[j].esq[k].cir.y) / distancia;
                        }
                    }


                }
            }
        }

        for (auto c : listPelEsq)
        {
            PELOTAS* p1 = c.first;
            ESQUINAS* p2 = c.second;

            distancia = distanciaEntrePuntos(p1->cir, p2->cir);

            float nx = (p2->cir.x - p1->cir.x) / distancia;
            float ny = (p2->cir.y - p1->cir.y) / distancia;

            float tx = -ny;
            float ty = nx;

            float dpTan1 = p1->vel.x * tx + p1->vel.y * ty;
            float dpTan2 = 0;

            float dpNorm1 = p1->vel.x * nx + p1->vel.y * ny;
            float dpNorm2 = 0;

            float m1 = (dpNorm1 * (p1->masa - p2->masa) + 2.0f * p2->masa * dpNorm2) / (p1->masa + p2->masa);
            float m2 = (dpNorm2 * (p2->masa - p1->masa) + 2.0f * p1->masa * dpNorm1) / (p1->masa + p2->masa);

            p1->vel.x = tx * dpTan1 + nx * m1;
            p1->vel.y = ty * dpTan1 + ny * m1;

            p1->enMovimiento = true;
        }
    }

    void colisionPelotaBanda()
    {
        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].activo)
            {
                for (int j = 0; j < maxBandas; j++)
                {
                    if (chequearColisionBorde(pelota[i].cir, pelota[i].radio, banda[j].rec))
                    {
                        std::cout << "ColisionPB: Pelota: " << i << " - Banda: " << j << std::endl;
                        switch (banda[j].tipo)
                        {
                        case BANDA::ARRIBA:
                            pelota[i].vel.y = fabsf(pelota[i].vel.y);
                            break;
                        case BANDA::ABAJO:
                            pelota[i].vel.y = -fabsf(pelota[i].vel.y);
                            break;
                        case BANDA::DERECHA:
                            pelota[i].vel.x = -fabsf(pelota[i].vel.x);
                            break;
                        case BANDA::IZQUIERDA:
                            pelota[i].vel.x = fabsf(pelota[i].vel.x);
                            break;
                        default:
                            std::cout << "Borde sin tipo" << std::endl;
                            system("pause");
                            break;
                        }
                    }
                }
            }
        }

    }

    void colisionPelotaHoyo()
    {
        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].activo)
            {
                for (int j = 0; j < maxHoyos; j++)
                {
                    if (ChequeoPuntoCirculo(pelota[i].cir, hoyo[j].pos, radioHoyos))
                    {
                        pelota[i].desaparicion = true;
                    }
                }
            }
        }

    }

    bool algunaPelotaEnMovimiento()
    {
        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i].enMovimiento)
            {
                return false;
            }
        }
        if (!pelota[0].activo)
        {
            pelota[0].activo = true;
            pelota[0].cir.x = posicionInicialBlanca.x;
            pelota[0].cir.y = posicionInicialBlanca.y;
            pelota[0].radio = radioGral;
            std::cout << "Pelota Blanca Spawneada\n";
        }
        return true;
    }

    float distanciaEntrePuntos(Vector2 p1, Vector2 p2)
    {
        return sqrt(pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2));
    }

    bool chequearColisionCirCir(Vector2 p1, int p1Radio, Vector2 p2, int p2Radio)
    {
        float distancia = distanciaEntrePuntos(p1, p2);

        if (distancia < p1Radio + p2Radio)
        {
            return true;
        }
        return false;
    }
    
    bool chequearColisionCirCir(float distancia, int p1Radio, int p2Radio)
    {
        if (distancia < p1Radio + p2Radio)
        {
            return true;
        }
        return false;
    }

    bool ChequeoPuntoCirculo(Vector2 point, Vector2 center, int radio)
    {
        float distancia = distanciaEntrePuntos(point, center);

        if (distancia <= radio)
        {
            return true;
        }
        return false;
    }

    bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec)
    {
        Vector2 p = posicionPel;

        if (p.x < rec.x)
        {
            p.x = rec.x;
        }
        else if (p.x > rec.x + rec.width)
        {
            p.x = rec.x + rec.width;
        }

        if (p.y < rec.y)
        {
            p.y = rec.y;
        }
        else if (p.y > rec.y + rec.height)
        {
            p.y = rec.y + rec.height;
        }

        float distancia = distanciaEntrePuntos(posicionPel, p);
        if (distancia < radio)
        {
            return true;
        }
        return false;
    }

    void initPalo()
    {
        palo.textura = LoadTexture("res/PaloPool.png");
        palo.pos = { 0, 0 };
        palo.rot = 0;
    }

    void updatePalo()
    {
        float ladoOpuesto = mouse.y - pelota[0].cir.y;
        float ladoAdyacente = mouse.x - pelota[0].cir.x;

        palo.rot = atan2(ladoOpuesto, ladoAdyacente) * 180 / PI;
    }

    void initPelotas()
    {
        int lisasEmbocadas = 0;
        int ralladasEmbocadas = 0;
        for (int i = 0; i < maxPelotas; i++)
        {
            pelota[i].activo = true;
            pelota[i].enMovimiento = false;
            pelota[i].vel = { 0, 0 };

            if (i < maxPelotas / 2)
            {
                pelota[i].tipo = TIPOPELOTA::LISA;
                pelota[i].color = RED;
            }
            else if (i > maxPelotas / 2)
            {
                pelota[i].tipo = TIPOPELOTA::RALLADA;
                pelota[i].color = BLUE;
            }
            else if (i == maxPelotas / 2)
            {
                pelota[i].tipo = TIPOPELOTA::NEGRA;
                pelota[i].color = BLACK;
            }
        }
        pelota[0].tipo = TIPOPELOTA::BLANCA;
        pelota[0].color = WHITE;
        pelota[0].cir.x = posicionInicialBlanca.x;
        pelota[0].cir.y = posicionInicialBlanca.y;

        //Ejemplo dibujado: https://docs.google.com/presentation/d/1M6j4hbTV_CXiAvEjFToSM-Od6RLlj7D448pQLye2ec4/edit#slide=id.p

        auxRaizDe3PorRadio = static_cast<int>(raizDe3 * radioGral);

        pelota[1].cir.x = posicionInicialUno.x;
        pelota[1].cir.y = posicionInicialUno.y;

        IniciarSeteo(9, 1);
        IniciarSeteo(2, 9);
        IniciarSeteo(10, 2);
        IniciarSeteo(3, 10);
        setearPelotaConRef(11, 3, 3);
        setearPelotaConRef(4, 11, 11);
        setearPelotaConRef(12, 11, 4);
        setearPelotaConRef(5, 11, 12);
        setearPelotaConRef(15, 10, 10);
        setearPelotaConRef(7, 15, 15);
        setearPelotaConRef(13, 7, 7);
        setearPelotaConRef(8, 2, 2);
        setearPelotaConRef(6, 8, 8);
        setearPelotaConRef(14, 9, 9);

    }

    void IniciarSeteo(int setear, int seteador)
    {
        pelota[setear].cir.x = pelota[seteador].cir.x + auxRaizDe3PorRadio + separacionBolas;
        pelota[setear].cir.y = pelota[seteador].cir.y - radioGral - 1;
    }
    
    void setearPelotaConRef(int setear, int enX, int enY)
    {
        pelota[setear].cir.x = pelota[enX].cir.x;
        pelota[setear].cir.y = pelota[enY].cir.y + radioGral * 2 + separacionBolas;
    }

    void initBordes()
    {
        fondo = LoadTexture("res/MesaPool.png");
        fondo.width = GetScreenWidth();
        fondo.height = GetScreenHeight();

        banda[0].tipo = BANDA::ARRIBA;
        banda[0].rec.width = 455;
        banda[0].rec.height = 62;
        banda[0].rec.x = 120;
        banda[0].rec.y = 0;

        banda[1].tipo = BANDA::ARRIBA;
        banda[1].rec.width = 460;
        banda[1].rec.height = 62;
        banda[1].rec.x = 700;
        banda[1].rec.y = 0;

        banda[2].tipo = BANDA::DERECHA;
        banda[2].rec.width = 60;
        banda[2].rec.height = 490;
        banda[2].rec.x = screenWidth - banda[2].rec.width;
        banda[2].rec.y = 115;

        banda[3].tipo = BANDA::ABAJO;
        banda[3].rec.width = banda[0].rec.width;
        banda[3].rec.height = 62;
        banda[3].rec.x = banda[1].rec.x;
        banda[3].rec.y = screenHeight - banda[3].rec.height;

        banda[4].tipo = BANDA::ABAJO;
        banda[4].rec.width = banda[0].rec.width;
        banda[4].rec.height = 62;
        banda[4].rec.x = banda[0].rec.x;
        banda[4].rec.y = screenHeight - banda[4].rec.height;

        banda[5].tipo = BANDA::IZQUIERDA;
        banda[5].rec.width = 60;
        banda[5].rec.height = banda[2].rec.height;
        banda[5].rec.x = 0;
        banda[5].rec.y = banda[2].rec.y;

        crearEsquinasH(0);
        crearEsquinasH(1);
        crearEsquinasH(3);
        crearEsquinasH(4);

        crearEsquinasV(2);
        crearEsquinasV(5);
    }

    void crearEsquinasH(int i)
    {
        banda[i].esq[0].radio = banda[i].rec.height / 2;
        banda[i].esq[0].cir = { banda[i].rec.x, banda[i].rec.y + banda[i].rec.height / 2 };

        banda[i].esq[1].radio = banda[i].esq[0].radio;
        banda[i].esq[1].cir = { banda[i].rec.x + banda[i].rec.width, banda[i].rec.y + banda[i].rec.height / 2 };
    }
    
    void crearEsquinasV(int i)
    {
        banda[i].esq[0].radio = banda[i].rec.width / 2;
        banda[i].esq[0].cir = { banda[i].rec.x + banda[i].rec.width / 2, banda[i].rec.y };

        banda[i].esq[1].radio = banda[i].esq[0].radio;
        banda[i].esq[1].cir = { banda[i].rec.x + banda[i].rec.width / 2, banda[i].rec.y + banda[i].rec.height };
    }

    void initHoyos()
    {
        hoyo[static_cast<int>(HOYO::ARRIBAIZQ)].pos = { margenH, margenV };
        hoyo[static_cast<int>(HOYO::ARRIBACEN)].pos = { screenWidth / 2 - 2, margenV };
        hoyo[static_cast<int>(HOYO::ARRIBADER)].pos = { screenWidth - margenH, margenV };
        hoyo[static_cast<int>(HOYO::ABAJOIZQ)].pos = { margenH, screenHeight - margenV };
        hoyo[static_cast<int>(HOYO::ABAJOCEN)].pos = { screenWidth / 2 - 2, screenHeight - margenV };
        hoyo[static_cast<int>(HOYO::ABAJODER)].pos = { screenWidth - margenH, screenHeight - margenV };
    }

    void dibujarHoyos(HOYOS cir)
    {
        DrawCircleLines(cir.pos.x, cir.pos.y, radioHoyos, cir.color);
    }

    void hacks()
    {
        if (IsKeyPressed(KEY_Q))
        {
            gameOver = !gameOver;
        }

        if (IsKeyPressed(KEY_SPACE))
        {   // ----- reinicio ------
            init();
        }
        if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
        {
            for (int i = 0; i < maxPelotas; i++)
            {
                if (ChequeoPuntoCirculo(mouse, pelota[i].cir, pelota[i].radio))
                {
                    pelota[i].cir = mouse;
                    break;
                }
            }
        }
    }
}*/