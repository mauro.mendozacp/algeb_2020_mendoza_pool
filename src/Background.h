#pragma once
#include "raylib.h"
#include <iostream>

#include "Borde.h"
#include "Hoyo.h"

namespace POOL {
namespace BACKGROUND {

	enum class HoyoUbicacion { ARRIBAIZQ, ARRIBACEN, ARRIBADER, ABAJOIZQ, ABAJOCEN, ABAJODER };

	extern Texture2D backgroundTexture;

	void init();
	void draw();
}
}