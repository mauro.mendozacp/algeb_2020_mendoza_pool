#include "raylib.h"
#include "Esquina.h"

namespace POOL {
	namespace BORDE {

		enum class BordeUbicacion { ARRIBA, ABAJO, DERECHA, IZQUIERDA };
		const int maxEsquinas = 2;
		const int maxBordes = 6;

		void deInit();

		class Borde
		{
		public:
			Borde();
			Borde(BordeUbicacion tipo, Rectangle rec);
			~Borde();

			void dibujar();

			BordeUbicacion getTipo();
			void setTipo(BordeUbicacion tipo);
			Rectangle getRec();
			void setRec(Rectangle rec);

			ESQUINA::Esquina* esq[maxEsquinas];

		private:
			BordeUbicacion tipo;
			Rectangle rec;
		};
		
		extern BORDE::Borde* borde[maxBordes];
	}
}