#pragma once
#include "raylib.h"

namespace POOL {
namespace COLLISION_MANAGER {

	void collisionManager();

	bool chequeoPuntoCirculo(Vector2 point, Vector2 center, float radio);

	void colisionPelotaPelota();
	void colisioPelotaEsquina();
	void colisionPelotaBorde();
	void colisionPelotaHoyo();

	float distanciaEntrePuntos(Vector2 p1, Vector2 p2);
	bool chequearColisionCirCir(float distancia, float p1Radio, float p2Radio);
	bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec);
}
}