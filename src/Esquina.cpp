#include "Esquina.h"
#include <iostream>

namespace POOL {
	namespace ESQUINA {

		Esquina::Esquina()
		{
			this->cir = { 0, 0 };
			this->radio = 0;
			this->masa = static_cast<float>(INT_MAX);
			this->color = BLUE;
		}

		Esquina::~Esquina()
		{
		}

		void Esquina::dibujar()
		{
			DrawCircleLines(cir.x, cir.y, radio, color);
		}

		Vector2 Esquina::getCir()
		{
			return this->cir;
		}

		void Esquina::setCir(Vector2 cir)
		{
			this->cir = cir;
		}

		float Esquina::getRadio()
		{
			return this->radio;
		}

		void Esquina::setRadio(float radio)
		{
			this->radio = radio;
		}

		float Esquina::getMasa()
		{
			return this->masa;
		}

		void Esquina::setMasa(float masa)
		{
			this->masa = masa;
		}

		Color Esquina::getColor()
		{
			return this->color;
		}

		void Esquina::setColor(Color color)
		{
			this->color = color;
		}
	}
}