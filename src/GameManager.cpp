#include "GameManager.h"

#include "raylib.h"

#include "Gameplay.h"

namespace POOL {
namespace GAME_MANAGER {

    static void init();
    static void update();
    static void draw();

    void runGame() {
        InitWindow(screenWidth, screenHeight, "2020_Pool_Algebra");
        SetTargetFPS(60);

        InitAudioDevice();

        init();

        while (!WindowShouldClose())
        {
            update();
            draw();
        }

        CloseWindow();
    }

    void init() {
        GAMEPLAY::init();
    }

    void update() {
        
        GAMEPLAY::update();
    }

    void draw() {
        BeginDrawing();
        ClearBackground(RAYWHITE);

        GAMEPLAY::draw();

        EndDrawing();
    }
}	
}