#pragma once
#include "raylib.h"

namespace POOL {
namespace GAMEPLAY {

	void init();
	void update();
	void draw();
	void deInit();

	extern int lisasEmbocadas;
	extern int ralladasEmbocadas;
	extern bool gameOver;

	extern Sound paloPelotaColSound;
	extern Sound pelotaPelotaColSound;
	extern Sound pelotaBordeColSound;
}
}