#include "Palo.h"

#include <iostream>
#include <cmath>

#include "Pelota.h"

namespace POOL {
    namespace PALO {

        Palo* palo;
        Texture2D paloTexture;

        void init()
        {
            paloTexture = LoadTexture("res/assets/PaloPool.png");
            palo = new Palo(paloTexture, { 0.0f,0.0f });
        }

        void update()
        {
            float ladoOpuesto = GetMousePosition().y - PELOTA::pelota[0]->getCir().y;
            float ladoAdyacente = GetMousePosition().x - PELOTA::pelota[0]->getCir().x;

            palo->setRot(atan2(ladoOpuesto, ladoAdyacente) * 180 / PI);
        }

        void draw()
        {
            palo->show();
        }

        void deInit()
        {
            if (palo != NULL)
            {
                delete palo;
                palo = NULL;
            }
            UnloadTexture(paloTexture);
        }

        Palo::Palo()
        {
            this->activo = false;
            this->textura = Texture2D();
            this->rot = 0.0f;
            this->pos = Vector2();
            this->endpos = Vector2();
        }

        Palo::Palo(Texture2D textura, Vector2 pos)
        {
            this->activo = false;
            this->textura = textura;
            this->rot = 0.0f;
            this->pos = pos;
            this->endpos = pos;
        }

        Palo::~Palo()
        {

        }

        const float auxMult = 100 / 90;
        void Palo::show()
        {
            Vector2 endpos = { PELOTA::pelota[0]->getCir().x + (GetMousePosition().x - PELOTA::pelota[0]->getCir().x) / 2, PELOTA::pelota[0]->getCir().y + (GetMousePosition().y - PELOTA::pelota[0]->getCir().y) / 2 };

            if (activo)
            {
                float porc = 0;
#if DEBUG
                DrawLineEx(PELOTA::pelota[0]->getCir(), GetMousePosition(), 1, WHITE);

                std::cout << "rot: " << rot << std::endl;
#endif
                //calculo de angulos con porcentaje
                if (rot > 0 && rot <= 90)
                {
                    porc = rot * 100 / 90;
                    endpos.x += (textura.height / 2) * porc / 100;

                    porc = 100 - (rot * 100 / 90);
                    endpos.y -= (textura.height / 2) * porc / 100;
                }
                if (rot <= 0 && rot >= -90)
                {
                    porc = abs(rot) * 100 / 90;
                    endpos.x -= (textura.height / 2) * porc / 100;

                    porc = 100 - (abs(rot) * 100 / 90);
                    endpos.y -= (textura.height / 2) * porc / 100;
                }

                if (rot < -90 && rot > -180)
                {
                    porc = 100 - ((abs(rot) - 90) * 100 / 90);
                    endpos.x -= (textura.height / 2) * porc / 100;

                    porc = ((abs(rot) - 90) * 100 / 90);
                    endpos.y += (textura.height / 2) * porc / 100;
                }
                if (rot <= 180 && rot > 90)
                {
                    porc = 100 - ((rot - 90) * 100 / 90);
                    endpos.x += (textura.height / 2) * porc / 100;

                    porc = ((rot - 90) * 100 / 90);
                    endpos.y += (textura.height / 2) * porc / 100;
                }

                DrawTextureEx(textura, endpos, rot, 1, WHITE);
            }
        }

        bool Palo::getActivo()
        {
            return this->activo;
        }

        void Palo::setActivo(bool activo)
        {
            this->activo = activo;
        }

        Texture2D Palo::getTextura()
        {
            return this->textura;
        }

        void Palo::setTextura(Texture2D textura)
        {
            this->textura = textura;
        }

        float Palo::getRot()
        {
            return this->rot;
        }

        void Palo::setRot(float rot)
        {
            this->rot = rot;
        }

        Vector2 Palo::getPos()
        {
            return this->pos;
        }

        void Palo::setPos(Vector2 pos)
        {
            this->pos = pos;
        }

        Vector2 Palo::getEndPos()
        {
            return this->endpos;
        }

        void Palo::setEndPos(Vector2 endPos)
        {
            this->endpos = endpos;
        }
    }
}