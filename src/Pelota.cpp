#include "Pelota.h"

#include <iostream>
#include <cmath>

namespace POOL {
namespace PELOTA {

	Pelota* pelota[maxPelotas];
	
    static void IniciarSeteo(int setear, int seteador);
    static void SetearPelotaConRef(int setear, int enX, int enY);

    void init()
    {
        for (int i = 0; i < maxPelotas; i++)
        {
            pelota[i] = new Pelota();
            pelota[i]->setActivo(true);
            pelota[i]->setEnMovimiento(false);
            pelota[i]->setVel({ 0, 0 });
            pelota[i]->setRadio(radioGral);

            if (i < maxPelotas / 2)
            {
                pelota[i]->setTipo(TipoPelota::LISA);
                pelota[i]->setColor(RED);
            }
            else if (i > maxPelotas / 2)
            {
                pelota[i]->setTipo(TipoPelota::RALLADA);
                pelota[i]->setColor(BLUE);
            }
            else if (i == maxPelotas / 2)
            {
                pelota[i]->setTipo(TipoPelota::NEGRA);
                pelota[i]->setColor(BLACK);
            }
        }
        pelota[0]->setTipo(TipoPelota::BLANCA);
        pelota[0]->setColor(WHITE);
        pelota[0]->setCir({ posicionInicialBlanca.x, posicionInicialBlanca.y });

        //Ejemplo dibujado: https://docs.google.com/presentation/d/1M6j4hbTV_CXiAvEjFToSM-Od6RLlj7D448pQLye2ec4/edit#slide=id.p

        pelota[1]->setCir({ posicionInicialUno.x, posicionInicialUno.y });

        IniciarSeteo(9, 1); //se define la posicion de la pelota 9, teniendo en cuenta la posicion de la 1
        IniciarSeteo(2, 9); //posicion de la pelota 2, teniendo en cuenta la posicion de la 9
        IniciarSeteo(10, 2); //posicion de la pelota 10, teniendo en cuenta la posicion de la 2
        IniciarSeteo(3, 10); //posicion de la pelota 3, teniendo en cuenta la posicion de la 10
        SetearPelotaConRef(11, 3, 3); //se define la posicion de la pelota 11, segun la "X" y la "Y" de la 3
        SetearPelotaConRef(4, 11, 11); //posicion de la pelota 4, segun la "X" y la "Y" de la 11
        SetearPelotaConRef(12, 11, 4); //posicion de la pelota 12, segun la "X" de la 11 y la "Y" de la 4
        SetearPelotaConRef(5, 11, 12); //posicion de la pelota 5, segun la "X" de la 11 y la "Y" de la 12
        SetearPelotaConRef(15, 10, 10); //posicion de la pelota 15, segun la "X" y la "Y" de la 10
        SetearPelotaConRef(7, 15, 15); //posicion de la pelota 7, segun la "X" y la "Y" de la 15
        SetearPelotaConRef(13, 7, 7); //posicion de la pelota 13, segun la "X" y la "Y" de la 7
        SetearPelotaConRef(8, 2, 2); //posicion de la pelota 8, segun la "X" y la "Y" de la 2
        SetearPelotaConRef(6, 8, 8); //posicion de la pelota 6, segun la "X" y la "Y" de la 8
        SetearPelotaConRef(14, 9, 9); //posicion de la pelota 14, segun la "X" y la "Y" de la 9
    }

    const float minVel = 0.1f;

	void update() 
    {

        for (int i = 0; i < maxPelotas; i++)
        {
            if (pelota[i]->getActivo() && pelota[i]->getEnMovimiento())
            {
                pelota[i]->mover();

                if (fabsf(pelota[i]->getVel().x) < minVel && fabsf(pelota[i]->getVel().y) < minVel)
                {   // ----- Que pare -----
                    std::cout << "Pelota " << i << " ha parado." << std::endl;
                    pelota[i]->setEnMovimiento(false);
                }
            }
        }
	}

	void draw()
    {

        for (int i = 0; i < maxPelotas; i++)
        {
            if (i > 0)
            {
                pelota[i]->dibujar(i);
            }
            else
            {
                pelota[i]->dibujar();
            }
        }
	}

    void deInit()
    {
    }

    Pelota::Pelota()
    {
        this->activo = false;
        this->tipo = TipoPelota::BLANCA;
        this->color = Color();

        this->cir = Vector2();
        this->radio = radioGral;
        this->vel = Vector2();
        this->masa = (float)radio;
        this->velDesgaste = PerdidaPotencia;

        this->enMovimiento = false;
        this->desaparicion = false;
    }

    Pelota::~Pelota()
    {
    }

    void Pelota::mover()
    {
        cir.x += vel.x;
        cir.y += vel.y;

        vel.x *= velDesgaste;
        vel.y *= velDesgaste;
    }

    void Pelota::dibujar()
    {
        //DrawCircle(cir.x, cir.y, radio, color);
        DrawCircleV(cir, radio, color);
    }

    void Pelota::dibujar(int numero)
    {
        dibujar();
        DrawText(TextFormat("%i", numero), (int)((cir.x - (MeasureText(TextFormat("%i", numero), (int)(radio)) / 2))), (int)(cir.y - radio / 2), (int)(radio), WHITE);
    }

    bool Pelota::getActivo()
    {
        return this->activo;
    }

    void Pelota::setActivo(bool activo)
    {
        this->activo = activo;
    }

    TipoPelota Pelota::getTipo()
    {
        return this->tipo;
    }

    void Pelota::setTipo(TipoPelota tipo)
    {
        this->tipo = tipo;
    }

    Color Pelota::getColor()
    {
        return this->color;
    }

    void Pelota::setColor(Color color)
    {
        this->color = color;
    }

    Vector2 Pelota::getCir()
    {
        return this->cir;
    }

    void Pelota::setCir(Vector2 cir)
    {
        this->cir = cir;
    }

    float Pelota::getRadio()
    {
        return this->radio;
    }

    void Pelota::setRadio(float radio)
    {
        this->radio = radio;
    }

    Vector2 Pelota::getVel()
    {
        return this->vel;
    }

    void Pelota::setVel(Vector2 vel)
    {
        this->vel = vel;
    }

    float Pelota::getMasa()
    {
        return this->masa;
    }

    void Pelota::setMasa(float masa)
    {
        this->masa = masa;
    }

    float Pelota::getVelDesgaste()
    {
        return this->velDesgaste;
    }

    void Pelota::setVelDesgaste(float velDesgaste)
    {
        this->velDesgaste = velDesgaste;
    }

    bool Pelota::getEnMovimiento()
    {
        return this->enMovimiento;
    }

    void Pelota::setEnMovimiento(bool enMovimiento)
    {
        this->enMovimiento = enMovimiento;
    }

    bool Pelota::getDesaparicion()
    {
        return this->desaparicion;
    }

    void Pelota::setDesaparicion(bool desaparicion)
    {
        this->desaparicion = desaparicion;
    }

    void IniciarSeteo(int setear, int seteador)
    {
        pelota[setear]->setCir({ pelota[seteador]->getCir().x + raizDe3PorRadio + separacionBolas, pelota[seteador]->getCir().y - radioGral - 1 });
    }

    void SetearPelotaConRef(int setear, int enX, int enY)
    {
        pelota[setear]->setCir({ pelota[enX]->getCir().x, pelota[enY]->getCir().y + radioGral * 2 + separacionBolas });
    }    
}
}